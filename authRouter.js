const Router = require('express')
const controller = require('./authController')
const { check } = require('express-validator')
const roleMiddleWare = require('./middleware/roleMiddleWare')

const router = new Router()

router.post('/registration', [
  check('username', "The user's name can't be empty").notEmpty(),
  check('password', "The password should be min 4 and max 12 symbols").isLength({ min: 4, max: 12 }),

], controller.registration)
router.post('/login', controller.login)
router.get('/users', roleMiddleWare(['ADMIN']), controller.getUsers)
router.post('/update', controller.update)
router.post('/uploadPicture', controller.uploadPicture)
router.get('/getPicture', controller.getPicture)
router.post('/updatePassword', controller.updatePassword)

module.exports = router

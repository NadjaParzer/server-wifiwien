const express = require('express')
const cors = require('cors') // Enable All CORS Requests
const mongoose  = require('mongoose') // Shell for MongoDB
const authRouter = require('./authRouter')
const fs = require( 'fs' ) // Filesystem
const authMiddleWare = require('./middleware/authMiddleWare')
const roleMiddleWare = require('./middleware/roleMiddleWare')
const fileUpload = require('express-fileupload')

const DB_URL = `mongodb+srv://nadjaP:040585@cluster0.m38bo.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`
const app = express();
const PORT = process.env.PORT ?? 7005

let pagenation = require('./functions/pagenation')
let sorting = require('./functions/sorting')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(fileUpload({
  createParentPath: true
}));

 async function startApp() {
  try {
    await mongoose.connect(DB_URL)
    app.listen(PORT, () => console.log(`Express Server auf PORT ${PORT} gestartet.`))
  } catch (err) {
    console.log(err)
  }
}
startApp()
app.use('/uploads', express.static('./uploads'))
app.use('/app/auth', authRouter)

let pfadCustomers = __dirname + '/data/customers.json'
let pfadIsos = __dirname + '/data/isos.json'
let pfadAgents = __dirname + '/data/agents.json'
let pfadDashboard = __dirname + '/data/dashboard.json'
let uploadFolder = __dirname + 'uploadFiles'


app.get( '/app/dashboard', authMiddleWare, (req,res)=>{
  fs.readFile( pfadDashboard, (err, data) => {
    let dashboardList = JSON.parse(data)
    res.send(dashboardList)
  })
})

app.get( '/app/customers', authMiddleWare, (req,res)=>{
  let page = req.query.page
  let number = req.query.number
  let sortLands = req.query.sortLands
  let sortCustomers = req.query.sortCustomers
  let sortDownloads = req.query.sortDownloads
  fs.readFile( pfadCustomers, (err, data) => {
    let customerList = JSON.parse(data)
    let customerListSorted

    if(sortLands != 'none') {
      customerListSorted = sorting.sortByProperty(customerList, sortLands, 'address', 'country')
    } else customerListSorted = customerList

    if(sortDownloads != 'none') {
      customerListSorted = sorting.sortByProperty(customerList, sortDownloads, 'downloadsMonth')
    } else customerListSorted = customerList

    if(sortCustomers != 'none') {
      customerListSorted = sorting.sortByProperty(customerList, sortCustomers, 'customerNumber')
    } else customerListSorted = customerList

    let customerListForRender = pagenation.getPage(customerListSorted, page, number)
    res.send({customers:customerListForRender,  totalCustomers: customerListSorted.length})
  })
})

app.get('/app/customers/search', authMiddleWare, (req, res) => {
  let queryCustomerName = req.query.customerName
  fs.readFile(pfadCustomers, (err, data) => {
    let customerList = JSON.parse(data)
    let status
    let customer = customerList.filter(customer => customer.customerName.toLowerCase().includes(queryCustomerName.toLowerCase()) )
    if( customer.length > 0 ) {
      status = 1
    } else {
      status = 0
    }
    res.send({resultCode: status, data: customer})
  })
})

app.get( '/app/isos', authMiddleWare, (req,res)=>{
  let page = req.query.page
  let number = req.query.number
  let sortID = req.query.sortID
  let sortDate = req.query.sortDate
  let sortSize = req.query.sortSize
  let sortDownloads = req.query.sortDownloads
  let sortTotalDuration = req.query.sortTotalDuration
  let sortMediumDuration = req.query.sortMediumDuration
  let sortReleaseDate = req.query.sortReleaseDate
  let sortStatus = req.query.sortStatus
  fs.readFile( pfadIsos, (err, data) => {
    let isoList = JSON.parse(data)
    let isoListSorted

    if(sortID != 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortID, 'id')
    } else isoListSorted = isoList
    if(sortDate != 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortDate, 'createdAt')
    } else isoListSorted = isoList
    if( sortSize!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortSize, 'size')
    } else isoListSorted = isoList
    if( sortDownloads!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortDownloads, 'downloads')
    } else isoListSorted = isoList
    if( sortTotalDuration!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortTotalDuration, 'totalDuration')
    } else isoListSorted = isoList
    if( sortMediumDuration!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortMediumDuration, 'averageDuration')
    } else isoListSorted = isoList
    if( sortReleaseDate!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortReleaseDate, 'dateOfPublish')
    } else isoListSorted = isoList
    if( sortStatus!= 'none') {
      isoListSorted = sorting.sortByProperty(isoList, sortStatus, 'status')
    } else isoListSorted = isoList

     let isoListForRender = pagenation.getPage(isoListSorted, page, number)
     res.send({isos: isoListForRender,  totalIsos: isoListSorted.length})
  })
})

app.get('/app/isos/search', authMiddleWare, (req, res) => {
  let queryIsoName = req.query.isoName
  fs.readFile(pfadIsos, (err, data) => {
    let isoList = JSON.parse(data)
    let status
    let isos = isoList.filter(iso => iso.id.toLowerCase().includes(queryIsoName.toLowerCase()) )
    if( isos.length > 0 ) {
      status = 1
    } else {
      status = 0
    }
    res.send({resultCode: status, data: isos})
  })
})

app.get('/app/agents', roleMiddleWare(['ADMIN']), (req, res) => {
  fs.readFile(pfadAgents, (err, data) => {
    let agentsList = JSON.parse(data)
    res.send({ agents: agentsList.agents })
    
  })
})

app.post('/app/release', roleMiddleWare(['ADMIN']), (req, res) => {
  const isoID = req.body.isoID
  fs.readFile(pfadIsos, (err, data) => {
     let isosForRelease = JSON.parse(data).map(iso => iso.id === isoID ? {...iso, status: 'Freigegeben'} : iso )
    fs.writeFile(pfadIsos, JSON.stringify(isosForRelease), ()=> {
      res.status(200).end( 'gespeichert!' );
    })
  })
})

const getPage = (array, page, number) => {

  let start = (page - 1) * number
  let end = start + number
  return array.slice(start, end)
}

module.exports = {
  getPage
}

let fs = require('fs')
let faker = require('faker')  // generate massive amounts of fake data

const generateCustomer = () => {
  return {
    id: faker.datatype.uuid(),
    customerNumber: faker.datatype.number(),
    customerName: faker.company.companyName(),
    lastIP: faker.internet.ip(),
    address: {
      country: faker.address.county(),
      city: faker.address.city(),
      street: faker.address.streetName(),
    },
    email: internet.email,
    contactPerson: `${faker.name.firstName} ${faker.name.lastName}`,
    phone: faker.phone.phoneNumber,
    isos: [
      {
        isoID: faker.datatype.uuid(),
        agent: faker.hacker.abbreviation,
        downloadStart: faker.date.past,
        downloadEnd: faker.date.past,
        dauer: faker.date.between
      }
    ]
  }
}

const generateCustomers = (number) => {
  return Array.from({ length: number }, generateCustomer)
}

module.exports = {
  generateCustomers: generateCustomers
}

// let dataObj = generateCustomers(2);
// fs.writeFileSync('data.json', JSON.stringify(dataObj, null, '\t'))

// const generateSinglCustomer = () => {
//   return {
//     id: faker.datatype.uuid(),
//     customerNumber: `1${faker.datatype.number()}`,
//     customerName: faker.company.companyName(),
//     lastIP: faker.internet.ip(),
//     downloadsMonth: faker.datatype.number(20),
//     address: {
//       country: faker.address.country(),
//       city: faker.address.city(),
//       street: faker.address.streetName(),
//     },
//     email: faker.internet.email,
//     contactPerson: `${faker.name.firstName()} ${faker.name.lastName()}`,
//     phone: faker.phone.phoneNumber(),
//     isos: [
//       {
//         isoID: faker.datatype.uuid(),
//         agent: faker.hacker.abbreviation(),
//         downloadStart: faker.date.past(),
//         downloadEnd: faker.date.past(),
//         dauer: faker.date.between()
//       },
//       {
//         isoID: faker.datatype.uuid(),
//         agent: faker.hacker.abbreviation(),
//         downloadStart: faker.date.past(),
//         downloadEnd: faker.date.past(),
//         dauer: faker.date.between()
//       },
//       {
//         isoID: faker.datatype.uuid(),
//         agent: faker.hacker.abbreviation(),
//         downloadStart: faker.date.past(),
//         downloadEnd: faker.date.past(),
//         dauer: faker.date.between()
//       },
//       {
//         isoID: faker.datatype.uuid(),
//         agent: faker.hacker.abbreviation(),
//         downloadStart: faker.date.past(),
//         downloadEnd: faker.date.past(),
//         dauer: faker.date.between()
//       },
//       {
//         isoID: faker.datatype.uuid(),
//         agent: faker.hacker.abbreviation(),
//         downloadStart: faker.date.past(),
//         downloadEnd: faker.date.past(),
//         dauer: faker.date.between()
//       }
//     ]
//   };
// };

// const generateCustomers = (number) => {
//   return Array.from({ length: number }, generateSinglCustomer);
// };

// let dataObj = generateCustomers(200);
// fs.writeFile(NewpfadCustomers, JSON.stringify(dataObj, null, '\t'), (err) => {
//   if(err){
//     return console.log(err)
//   }
//   console.log("The file is saved!")
// });

// const generateSingIso = () => {
//   return {
//     id: faker.datatype.uuid(),
//     createdAt: faker.date.past(),
//     size: faker.datatype.number(10),
//     downloads: faker.datatype.number(8000),
//     totalDuration: faker.datatype.number(180, 250),
//     averageDuration: faker.datatype.number(180, 200),
//     customerName: faker.company.companyName(),
//     status: faker.datatype.string(),
//     dateOfPublish: faker.date.past()
// }
// }

// const generateIsos = (number) => {
//   return Array.from({ length: number }, generateSingIso);
// };

// let dataObj = generateIsos(100);
// fs.writeFile(pfadIsos, JSON.stringify(dataObj, null, '\t'), (err) => {
//   if(err){
//     return console.log(err)
//   }
//   console.log("The file is saved!")
// });

const sortByProperty = (array, sortFactor, property1, property2) => {
  let sortedByPropertyArray
  if (property2) {
    sortFactor === 'AZ' ? sortedByPropertyArray = array.sort((a, b) => a[property1][property2] > b[property1][property2] ? 1 : -1)
      : sortedByPropertyArray = array.sort((a, b) => a[property1][property2] > b[property1][property2] ? -1 : 1)
  } else {
    sortFactor === 'AZ' ? sortedByPropertyArray = array.sort((a, b) => +a[property1] > +b[property1] ? 1 : -1)
      : sortedByPropertyArray = array.sort((a, b) => +a[property1] > +b[property1] ? -1 : 1)
  }
  return sortedByPropertyArray
}

module.exports = {
  sortByProperty
}

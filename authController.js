const User = require('./models/User')
const Role = require('./models/Role')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { validationResult } = require('express-validator')
const { secret } = require('./config')
const path = require('path')

const generateAccessToken = (id, roles) => {
  const payload = {
    id,
    roles
  }
  return jwt.sign(payload, secret, { expiresIn: "24h" })
}

class authController {

  async registration(req, res) {
    try {
      const errors = validationResult(req)
      if (!errors.isEmpty()) {
        return res.status(400).json({ message: "Registration failed", errors })
      }
      const { username, password } = req.body
      const candidate = await User.findOne({ username })
      if (candidate) {
        return res.status(400).json({ message: `User with name ${username} already exist` })
      }
      const hashPassword = bcrypt.hashSync(password, 4)
      const userRole = await Role.findOne({ value: 'USER' })
      const user = new User({ ...req.body, password: hashPassword, roles: [userRole.value] })
      await user.save()
      return res.json({ message: 'The registration is done!' })
    } catch (e) {
      console.log(e)
      res.status(400).json({ message: 'Registration error!' })
    }
  }

  async login(req, res) {
    try {
      const { username, password } = req.body
      const user = await User.findOne({ username })
      if (!user) {
        return res.json({ status: 400, message: 'User is not found!' })
      }
      const validPasswort = bcrypt.compareSync(password, user.password)
      if (!validPasswort) {
        return res.json({ status: 400, message: "The password is not correct!" })
      }
      const token = generateAccessToken(user._id, user.roles)
      console.log('LOGIN USER', user)
      return res.json({ token, mail: user.username, firstname: user.firstname, lastname: user.lastname, country: user.country, city: user.city, phone: user.phone, position: user.position, roles: user.roles, avatar: user.avatar })

    } catch (e) {
      console.log(e)
      res.status(400).json({ message: 'Login error!' })
    }
  }

  async getUsers(req, res) {
    try {
      // 1st request - creating Roles
      // const userRole = new Role()
      // const adminRole = new Role({value: 'ADMIN'})
      // await userRole.save()
      // await adminRole.save()
      const users = await User.find()
      res.json(users)
      res.json('Server is working!')
    } catch (e) {
      console.log(e)
    }
  }

  async update(req, res) {
    try {
      const { username, mail, firstname, lastname, phone, country, city, position } = req.body
      const user = await User.findOne({ username })
      console.log('UPDATE USER', username)
      if (!user) {
        return res.status(400).json({ message: 'User is not found!' })
      }
      user.username = mail
      user.firstname = firstname
      user.lastname = lastname
      user.phone = phone
      user.country = country
      user.city = city
      user.position = position
      user.save()
      user.updateOne({ username: username }, { $set: { ...req.body } })
      return res.json({ mail: user.username, firstname: user.firstname, lastname: user.lastname, country: user.country, city: user.city, phone: user.phone, position: user.position, roles: user.roles, avatar: user.avatar })
    } catch (e) {
      console.log(e)
      res.status(400).json(e)
    }
  }

  async uploadPicture(req, res) {
    try {
      const username = req.body.username
      let avatar = req.files.picture.name
      let fileName = Date.now() + path.extname(avatar)
      req.files.picture.mv('./uploads/' + fileName);
      const user = await User.findOne({ username })
      if (!user) {
        return res.status(400).json({ message: 'User is not found!' })
      }
      user.avatar = fileName
      user.save()
      res.send({
        status: true,
        message: 'File is uploaded',
        data: {
          name: avatar.name,
          mimetype: avatar.mimetype,
          size: avatar.size
        }
      });
    } catch (e) {
      console.log(e)
      res.status(500).json({ message: "Upload error" })
    }
  }

  async getPicture(req, res) {
    const username = req.query.username
    console.log('updatePicture')
    const user = await User.findOne({ username })
    if (!user) {
      return res.status(400).json({ message: 'User is not found!' })
    }
    res.json({ avatar: user.avatar })
  }

  async updatePassword(req, res) {
    const username = req.body.username
    const newPassword = req.body.password
    const user = await User.findOne({ username })
    if (!user) {
      return res.status(400).json({ message: 'User is not found!' })
    }
    const hashPassword = bcrypt.hashSync(newPassword, 4)
    user.password = hashPassword
    await user.save()
    res.json({ message: 'Password is changed!' })
  }
}

module.exports = new authController()

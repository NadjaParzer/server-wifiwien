const { Schema, model, isValidObjectId } = require('mongoose')

const User = new Schema({
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  firstname: { type: String },
  lastname: { type: String },
  avatar: { type: String },
  phone: { type: String },
  country: { type: String },
  city: { type: String },
  position: { type: String },
  roles: [{ type: String, ref: 'Role' }],
  // diskSpace: {type: Number, default: 1024*3*10},
  // usedSpace: {type: Number, default: 0},
  // files:[{type: isValidObjectId, ref: 'File'}]
})

module.exports = model('User', User)
